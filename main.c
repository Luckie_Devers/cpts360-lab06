#include <stdio.h>
#include <string.h>
#include "hello.h"

void activate_new_feature() {
	printf("Activating new feature...\n");
	// This either will look blue when printed or just look like a bunch of weird text
	// if your terminal somehow doesn't support ANSI codes.
	printf("\x1b[36mNew Feature activated\x1b[0m\n");
}

int main(int argc, char** argv) {
    char name[100];  // buffer for the name

    printf("Please enter your name: ");
    fflush(stdout); // Explicitly flush stdout to ensure the prompt is displayed

    fgets(name, sizeof(name), stdin);  // Read the name from standard input

    // Remove newline character if fgets reads it from the input
    if ((strlen(name) > 0) && (name[strlen(name) - 1] == '\n')) {
        name[strlen(name) - 1] = '\0';
    }

    say_hello(name);

    return 0;
}

	activate_new_feature();
	say_hello("cow");
}
